package sdk;
import java.io.IOException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Extenttest_Report  
{
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test;


	@BeforeSuite
	public void setUp()
	{
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") +"/SDKTest_Execution_Report.html");
		System.out.println(System.getProperty("user.dir"));
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		extent.setSystemInfo("OS", "Mac");
		extent.setSystemInfo("Host Name", "Vijay");
		extent.setSystemInfo("Environment", "Production");
		extent.setSystemInfo("User Name", "Qa-Team");

		htmlReporter.config().setChartVisibilityOnOpen(true);
		htmlReporter.config().setDocumentTitle("AutomationTesting.in Report");
		htmlReporter.config().setReportName("Smoke Test");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.DARK);
	}

	@AfterMethod
	public void getResult(ITestResult result) throws IOException
	{

		if(result.getStatus() == ITestResult.FAILURE)
		{
			System.out.println("capturreeeeeee");

			test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			
			String temp = FunctionLibrary.captureScreen(BaseClass.androidDriver);
			test.fail(result.getThrowable(), MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
			
			//test.fail(result.getThrowable());
			//captureScreen();
		}
		else if(result.getStatus() == ITestResult.SUCCESS)
		{
			//test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			//String temp = AppiumTest.captureScreen();
			//test.log(Status.PASS, "details", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());

			//test.pass(result.getThrowable(), MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
		}
		else
		{
			test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			String temp = FunctionLibrary.captureScreen(BaseClass.androidDriver);
			test.skip(result.getThrowable(), MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
			}
	}

	//    public static String getScreenhot(AndroidDriver driver, String screenshotName) throws Exception 
	//    {
	//    	 String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
	//    	 TakesScreenshot ts = (TakesScreenshot) driver;
	//    	 File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	//    	                //after execution, you could see a folder "FailedTestsScreenshots" under src folder
	//    	 String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/"+ screenshotName+dateName+".png";
	//    	 File finalDestination = new File(destination);
	//    	 FileUtils.copyFile(scrFile, finalDestination);
	//    	 return destination;
	//    }



	@AfterSuite
	public void tearDown()
	{   
		System.out.println("Inside ExtentFlush");
		extent.flush();
		FunctionLibrary.stopServer();
		//mail();

	}
	
	


}



