package sdk;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.google.common.collect.ImmutableMap;

import dataProvider.ConfigFileReader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class FunctionLibrary extends BaseClass {
	private static final String CHAR_LIST = 
	        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	    private static final int RANDOM_STRING_LENGTH = 10;
	    public static ConfigFileReader con = new ConfigFileReader();
	
	public void click(MobileElement element) throws Exception {
		try {
			element.click();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public String getText(MobileElement element) {
		String text = "";
		try {
			text = element.getText();
			return text;
		} catch (Exception e) {
			e.printStackTrace();
			return text;
		}
	}
	
	public void sendText(MobileElement element) {
		try {
			
			element.clear();
			String st=generateRandomString();
			element.sendKeys("vj");
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
	}
	
	

	public boolean waitForElementPresence(MobileElement element) {
		try {


			WebDriverWait expWait = new WebDriverWait(androidDriver,10);
			expWait.until(ExpectedConditions.visibilityOf(element));
			//boolean isElementPresent = element.isDisplayed();
			System.out.println("Element is displayed");
			return true;	

		} catch (Exception e) {
			System.out.println("Exception at watiforelement fucntion is:"+e);
			return false;
		}

	}

	public void alert_handle(MobileElement element) throws Exception {
		try {

			WebDriverWait expWait = new WebDriverWait(androidDriver,10);
			expWait.until(ExpectedConditions.visibilityOf(element));
			element.click();
		} catch (Exception e) {

		}
	}

	public boolean is_empty(List<MobileElement> list) throws Exception {
		try {

			//List<MobileElement> events_list = element.findElementsById("com.livelike.livelikedemo:id/widgets_only_button");


			if(list.isEmpty())
			{
				System.out.println("list Elements are not displayed");
				return true;
			}
			else
			{
				System.out.println("list Elements are displayed");
				return false;

			}

		} catch (Exception e) {
			System.out.println("Exception at is_empty fucntion is:"+e);
			return false;
		}

	}
	
	
	//Return The size of elements present on the screen.
	public int elemetns_Size(List<MobileElement> list) throws Exception {
		try {
				return list.size();	
			}
            catch (Exception e) {
			System.out.println("Exception at is_empty fucntion is:"+e);
			return 0;
            }
		 
		}

	


	//Taking ScreenShot
	public static String captureScreen(AppiumDriver<MobileElement> driver) throws IOException
	{
		System.out.println("capture screen fun is called!!!");
		TakesScreenshot screen = (TakesScreenshot)driver;
		File src = screen.getScreenshotAs(org.openqa.selenium.OutputType.FILE);
		//File src = screen.getScreenshotAs(OutputType.FILE);
		// Open the current date and time
		String timestamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		String dest ="/Users/livelike/eclipse-workspace/appiumworkspace/SDK_PageFactoryModel/src/test/java/screenshots/"+"Screenshot:"+timestamp+".png";
		File target = new File(dest);
		FileUtils.copyFile(src, target);
		return dest;

	}

	//verify the reuslt for extent test report
	public static void verifyText(boolean flag, boolean b, String key, ExtentTest test) throws IOException 
	{
		try {
			Assert.assertEquals(flag, b);
			logResult(test, key, flag, b, "PASS");
	

		} catch (Error e) {
			System.out.println("inside catch");
			logResult(test, key, flag, b, "FAIL");
			test.fail("Exception is: "+e);

		}

	}

	//To log the result in extent test report
	public static void logResult(ExtentTest test, String key, Boolean sent, boolean b, String result) throws IOException {
		if (result.equalsIgnoreCase("PASS")) {
			test.log(Status.PASS,"<font size=3 color=\"#3ADF00\">" + key + "</font>" + "  Actual: " + sent + " ||   Expected: " + b);
			String temp = captureScreen(androidDriver);
			test.log(Status.PASS, "details", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());


		} else {
			
			test.log(Status.FAIL, "<font size = 3 color=\"#FE2E2E\">" + key + "</font>" + " Actual: " + sent  + "||   Expected: " + b);
			String temp = captureScreen(androidDriver);
			test.log(Status.FAIL, "details", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());

			//getScreenshot(key, test);
		}
	}

	public static void stopServer() {
		Runtime runtime = Runtime.getRuntime();
		try {
			//androidDriver.quit();
			runtime.exec("killall node");
			System.out.println("Inside stopserver");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void appRelaunch() {
		// TODO Auto-generated method stub
		androidDriver.closeApp();
		androidDriver.launchApp();

	}
	
	public static boolean compareDateTime(String videoTimeStamp1, String videoTimeStamp2) throws ParseException {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	       Date date1 =  sdf.parse(videoTimeStamp1);
	       Date date2 =  sdf.parse(videoTimeStamp2);
	       long timestamp = date2.getTime()-date1.getTime();
	       if (timestamp>=2000) 
	       {
	    	   System.out.println("Timestamp: "+timestamp);
	    	   return true;
	       }
	       else 
	       {
	    	   System.out.println("Timestamp: "+timestamp);
	    	   return false;
	       }
	           }
	
	 /**
     * This method generates random string
     * @return
     */
    public static String generateRandomString(){
         
        StringBuffer randStr = new StringBuffer();
        for(int i=0; i<RANDOM_STRING_LENGTH; i++){
            int number = getRandomNumber();
            char ch = CHAR_LIST.charAt(number);
            randStr.append(ch);
        }
        return randStr.toString();
    }
    
    /**
     * This method generates random numbers
     * @return int
     */
    private static int getRandomNumber() {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(CHAR_LIST.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }
    
    
    /**
     * This method enters text in chat screen using processbuilder
     * @param text 
     * @param udid 
     * 
     */
    public static void sendInputOnChatScreen(String udid, String text) throws Exception {
		// TODO Auto-generated method stub
    	
		for(int i=0;i<=5;i++)
		{
				try {
		     new ProcessBuilder(new String[]{"adb", "-s",udid, "shell", "input", "text","xyz"})
		       .redirectErrorStream(true)
		       .start();
		     Thread.sleep(2000);
		     androidDriver.executeScript("mobile: performEditorAction", ImmutableMap.of("action", "send"));
		   //chatBox.setValue("Meenal");
				//Thread.sleep(2000);
				//return false;
		     
		} catch (IOException e) {
		   e.printStackTrace();
		}		
		}
    }
		
		
		
		
		
	  
	}
    
	






