package sdk;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import dataProvider.ConfigFileReader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class BaseClass extends Extenttest_Report{

	protected static AppiumDriver<MobileElement> androidDriver;
	public static String currentApp = "mobile";
	public static int waitTime;
	public static String PASS = "PASS";
	public static String FAIL = "FAIL";
	public static String INFO = "INFO";
	protected static AppiumDriverLocalService service;
	protected static AppiumServiceBuilder builder;
	protected static String deviceName;
	protected static String platformVersion;
	protected static String platformName;
	protected static String automationName;
	protected static boolean noReset;
	protected static String nodeExePath;
	protected static String appiumMainJSPath;
	public static WebDriverWait expWait;
	public static HashMap<String, String> elements = new HashMap<String, String>();
	public String methodName;
	public Boolean hasFailed = false;
	ConfigFileReader con = new ConfigFileReader();
	DesiredCapabilities caps;
	
	@BeforeTest(alwaysRun = true)
	public void setup() throws IOException {
		caps= new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.NO_RESET, con.getNoResetValue());
		caps.setCapability("deviceName", "S9");
		caps.setCapability(MobileCapabilityType.UDID, con.device_Udid());
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, con.platformName());
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, con.platform_Version());
		// cap.setCapability("appPackage", "com.livelike.livelikedemo");
		// cap.setCapability("appActivity", ".MainActivity");
		caps.setCapability(MobileCapabilityType.APP, con.getApplcationName());
		caps.setCapability("autoGrantPermissions", con.get_autoGrantPermissions());
		caps.setCapability("autoAcceptAlerts", con.get_autoAcceptAlerts());
		// Instantiate Appium Driver

		caps.setCapability("ignoreUnimportantViews", true);
		caps.setCapability("disableAndroidWatchers", true);
		
		caps.setCapability("clearSystemFiles", con.get_clearSystemFiles());
		// cap.setCapability("shouldUseTestManagerForVisibilityDetection", true);
		caps.setCapability("newCommandTimeout", con.get_newCommandTimeout());

		if (checkIfServerIsRunnning(con.get_port()))
		{
			System.out.println("checkIfServerIsRunnning");
			connect();
		}
		else
		{
			try {
				System.out.println("inside builder process");
				builder = new AppiumServiceBuilder();
				builder.withIPAddress("127.0.0.1");
				builder.usingPort(4723);
				builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
				builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");
				service = AppiumDriverLocalService.buildService(builder);
				service.start();
				androidDriver = new AndroidDriver<MobileElement>(service, caps);

			} catch (Exception e) {
				System.out.println("inside catch: "+e);
				FunctionLibrary.stopServer();
			}
			androidDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			expWait = new WebDriverWait(androidDriver, waitTime);
		}
	}


	private void connect() throws MalformedURLException {
		// TODO Auto-generated method stub

		System.out.println("Just connect but save the code for launching the already created server");
		//LOGGER.info("Connecting to existing up Appium service with empty driver...");
		androidDriver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), caps);


	}

	

	private boolean checkIfServerIsRunnning(int port) {
		try {
			//Appium is running if no Exception occurs.
			new Socket("0.0.0.0", port).close();
			System.out.println("Appium server is running on port:4723");
			return true;
		} catch (Exception e) {
			// No appium server is running
			System.out.println("Appium server is not running on port:4723");
			return false;
		}
	}

    @AfterTest(alwaysRun = true)
	public void teardown() {
		System.out.println("Inside Teardown......................");
		androidDriver.quit();
		
	}

}
