package sdkTestappModuleElements;



import java.io.IOException;
import java.net.ServerSocket;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
 
public class New{
 
	private AppiumDriverLocalService service;
	private AppiumServiceBuilder builder;
	private DesiredCapabilities cap;
	
	public void startServer() throws InterruptedException {
		
		//Set Capabilities
				cap = new DesiredCapabilities();
				cap.setCapability("noReset", "false");
				cap.setCapability("deviceName", "My Mobile Device");
		        cap.setCapability("udid","213581e4e60b7ece");
		        cap.setCapability("platformVersion", "9");
		        cap.setCapability("platformName", "Android");
		        cap.setCapability("appPackage", "com.livelike.livelikedemo");
		        cap.setCapability("appActivity", ".MainActivity");
		        cap.setCapability("autoGrantPermissions", true);
		        cap.setCapability("autoAcceptAlerts", true);
				//caps.setCapability("noReset", true);
				//caps.setCapability("unlockType", "pin");
				//caps.setCapability("unlockKey", "808080");
				//Instantiate Appium Driver
		        cap.setCapability("useNewWDA", true);
		        cap.setCapability("shouldUseSingletonTestManager", false);
		        cap.setCapability("clearSystemFiles", true);
		        cap.setCapability("shouldUseTestManagerForVisibilityDetection", true);
		        cap.setCapability("newCommandTimeout", 120);
				
		
		
		//Build the Appium service
		builder = new AppiumServiceBuilder();
		builder.withIPAddress("127.0.0.1");
		builder.usingPort(4723);
		builder.withCapabilities(cap);
		builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
		builder.withArgument(GeneralServerFlag.LOG_LEVEL,"error");
		
		//Start the server with the builder
		service = AppiumDriverLocalService.buildService(builder);
		service.start();
		AppiumDriver<MobileElement> driver =new AppiumDriver<>(service,cap);
		driver.findElementById("com.livelike.livelikedemo:id/layout_side_panel").click();
		Thread.sleep(5000);
		driver.quit();
	}
	
	public void stopServer() {
		service.stop();
	}
 
	public boolean checkIfServerIsRunnning(int port) {
		
		boolean isServerRunning = false;
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(port);
			serverSocket.close();
		} catch (IOException e) {
			//If control comes here, then it means that the port is in use
			isServerRunning = true;
		} finally {
			serverSocket = null;
		}
		return isServerRunning;
	}	
 
	public static void main(String[] args) throws InterruptedException {
		New appiumServer = new New();
		
		int port = 4723;
		if(!appiumServer.checkIfServerIsRunnning(port)) {
			appiumServer.startServer();
			appiumServer.stopServer();
		} else {
			System.out.println("Appium Server already running on Port - " + port);
		}
	}
}
