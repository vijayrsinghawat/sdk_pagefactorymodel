package sdkTestappModuleElements;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HomeScreenElements extends sdk.FunctionLibrary{


	@AndroidFindBy(id="com.livelike.livelikedemo:id/layout_side_panel")
	MobileElement drawerLayout;
	
	@AndroidFindBy(id="com.livelike.livelikedemo:id/edittext_chat_message")
	MobileElement edittextbox;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/layout_overlay")
	MobileElement layout_overlay;
	
	@AndroidFindBy(id="android:id/button1")
	MobileElement alertOK;
	
	@AndroidFindBy(id="com.livelike.livelikedemo:id/events_button")
	MobileElement events_button;
	
	@AndroidFindBy(id="com.livelike.livelikedemo:id/channel_list")
	MobileElement channel_list;
		
	@AndroidFindBy(id="com.livelike.livelikedemo:id/widgets_only_button")
	List<MobileElement> events_list;
	
	@AndroidFindBy(id="com.livelike.livelikedemo:id/widgets_only_button")
	MobileElement widgets_only_button;
	
	@AndroidFindBy(id="com.livelike.livelikedemo:id/standalone_section")
	List<MobileElement> standalone_section;
	
	@AndroidFindBy(id="com.livelike.livelikedemo:id/chat_only_button")
	MobileElement chat_only_button;
	
	

	public HomeScreenElements(AppiumDriver<MobileElement> driver) throws IOException {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		
		// TODO Auto-generated constructor stub
	}

	public boolean isEditTextboxDisplayed()
	{
		
	  boolean textbox=waitForElementPresence(edittextbox);
	  return textbox;
	}

	public void clickOnDrawerLayout() {
		try {
			click(drawerLayout);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void clickOverLayLayout() {
		try {
			click(layout_overlay);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void alertOk() {
		try {
			alert_handle(alertOK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clickOnEvents() {
		try {
			click(events_button);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean isChannelListEmpty() throws Exception
	{
		
	  boolean textbox=is_empty(events_list);
	  return textbox;
	}
	
	public void clickOnWidgetsOnly() {
		try {
			click(widgets_only_button);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean isWidgetOnlyListEmpty() throws Exception
	{
		
	  boolean textbox=is_empty(standalone_section);
	  return textbox;
	}
	
	public void clickOnChatOnly() {
		try {
			click(chat_only_button);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

}
