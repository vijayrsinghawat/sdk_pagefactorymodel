package sdkTestappModuleElements;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import sdk.FunctionLibrary;

public class AllLayoutScreenElements extends FunctionLibrary {
	// PageFactory - OR

	@AndroidFindBy(id="com.livelike.livelikedemo:id/startAd")
	MobileElement startAd;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/selectChannelButton")
	MobileElement channel;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/button3")
	MobileElement nothing;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/logsPreview")
	MobileElement logsPreview;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/openLogs")
	MobileElement logs;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/button_emoji")
	MobileElement butnEmoji;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/button_chat_send")
	MobileElement send;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_play")
	MobileElement play;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_pause")
	MobileElement pause;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_ffwd")
	MobileElement forward;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_next")
	MobileElement next;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_rew")
	MobileElement rewind;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_prev")
	MobileElement prev;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_position")
	MobileElement exoPosition;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_progress")
	MobileElement exoProgress;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_duration")
	MobileElement exoDuration;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/videoTimestamp")
	MobileElement vidTimeStamp;

	@AndroidFindBy(id="android:id/title")
	MobileElement channelTitle;
	//not found

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_subtitles")
	MobileElement exo_subtitles;


	@AndroidFindBy(id="com.livelike.livelikedemo:id/edittext_chat_message")
	MobileElement chatBox;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/chatMessage")
	List<MobileElement>chatId;


	@AndroidFindBy(id="com.livelike.livelikedemo:id/chatBackground")
	MobileElement chatBackground;
	//not found

	@AndroidFindBy(id="com.livelike.livelikedemo:id/chat_view")
	MobileElement chat_view;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/chatdisplay")
	MobileElement chatdisplay;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/action_bar_root")
	MobileElement action_bar_root;

	@AndroidFindBy(id="android:id/content")
	MobileElement content;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/playerView")
	MobileElement playerView;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_overlay")
	MobileElement exo_overlay;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/exo_content_frame")
	MobileElement exo_content_frame;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/chatWidget")
	MobileElement chatWidget;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/chatInput")
	MobileElement chatInput;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/chat_input_border")
	MobileElement chat_input_border;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/layout_side_panel")
	MobileElement drawerLayout;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/edittext_chat_message")
	MobileElement edittextbox;

	@AndroidFindBy(id="com.livelike.livelikedemo:id/widgets_only_button")
	List<MobileElement> events_list;





	public AllLayoutScreenElements(AppiumDriver<MobileElement> driver) throws IOException {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);

		// TODO Auto-generated constructor stub
	}


	public void click_startAd() {
		try {
			click(startAd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String gettext_startAd() {

		try {
			return getText(startAd);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public boolean is_startAd_enabled() {

		try {
			return waitForElementPresence(startAd);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	} 

	public void click_selectChannelButton() {

		try {
			click(channel);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getext_selectChannelButton() {

		try {
			channel.getText();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean is_selectChannelButton_enabled() {

		try {
			return channel.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	} 


	public void click_button3() {

		try {
			nothing.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getext_button3() {

		try {
			nothing.getText();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean is_button3_enabled() {

		try {
			return nothing.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	} 


	public boolean is_logsPreview_enabled() {

		try {
			return logsPreview.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	} 


	public void click_openLogs() {

		try {
			logs.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getext_openLogs() {

		try {
			logs.getText();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean is_openLogs_enabled() {

		try {
			return logs.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	} 


	public void click_button_emoji() {

		try {
			butnEmoji.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_button_emoji_enabled() {

		try {
			return butnEmoji.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void click_button_chat_send() {

		try {
			send.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_button_chat_send_enabled() {

		try {
			return send.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void click_exo_play_button() {

		try {
			play.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_exo_play_enabled() {

		try {
			return play.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public void clickExoPauseButton() {

		try {
			//click(exo_overlay);
			exo_overlay.click();
			pause.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_exo_pause_enabled() {

		try {
			return pause.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public void click_exo_ffwd_button() {

		try {
			forward.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_exo_ffwd_enabled() {

		try {
			return forward.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void click_exo_next_button() {

		try {
			next.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_exo_next_enabled() {

		try {
			return next.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void click_exo_rew_button() {

		try {
			rewind.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_exo_rew_enabled() {

		try {
			return rewind.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void click_exo_prev_button() {

		try {
			rewind.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_exo_prev_enabled() {

		try {
			return rewind.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void getext_exo_position() {

		try {
			exoPosition.getText();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_exo_position_enabled() {

		try {
			return exoPosition.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void getext_exo_duration() {

		try {
			exoDuration.getText();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_exo_duration_enabled() {

		try {
			return exoDuration.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void getext_videoTimestamp() {

		try {
			vidTimeStamp.getText();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean is_videoTimestamp_enabled() {

		try {
			return vidTimeStamp.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public boolean is_exo_subtitles_enabled() {

		try {
			return exo_subtitles.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void click_edittext_chat_message() {

		try {
			chatBox.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getext_edittext_chat_message() {

		try {
			chatBox.getText();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean is_edittext_chat_message_enabled() {

		try {
			return chatBox.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	} 


	public boolean is_chat_view_enabled() {

		try {
			return chat_view.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean is_chatdisplay_enabled() {

		try {
			return chatdisplay.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean is_action_bar_root_enabled() {

		try {
			return action_bar_root.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}



	public boolean is_content_enabled() {

		try {
			return content.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public boolean is_playerView_enabled() {

		try {
			return playerView.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean is_exo_overlay_enabled() {

		try {
			return exo_overlay.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	} 


	public boolean is_exo_content_frame_enabled() {

		try {
			return exo_content_frame.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	} 

	public boolean is_chatWidget_enabled() {

		try {
			return chatWidget.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	} 

	public boolean is_chatInput_enabled() {

		try {
			return chatInput.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean is_chat_input_border_enabled() {

		try {
			return chat_input_border.isEnabled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void clickOnDrawerLayout() {
		try {
			click(drawerLayout);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isEditTextboxDisplayed()
	{

		boolean textbox=waitForElementPresence(edittextbox);
		return textbox;
	}

	public boolean isChannelListEmpty() throws Exception
	{

		boolean textbox=is_empty(events_list);
		return textbox;
	}


	public void clickOnSyncChannel() throws Exception {
		// TODO Auto-generated method stub
		System.out.println(events_list.size());
		for(int i=0;i<events_list.size();i++)
		{
			if(events_list.get(i).getText().equalsIgnoreCase("ANDROID DEMO CHANNEL 2 SYNC"))
			{
				click(events_list.get(i));
				break;
			}

		}


	}


	public void clickOnNonSyncChannel() throws Exception {
		// TODO Auto-generated method stub
		System.out.println(events_list.size());
		for(int i=0;i<events_list.size();i++)
		{
			if(events_list.get(i).getText().equalsIgnoreCase("ANDROID DEMO CHANNEL 1 NO SYNC"))
			{
				click(events_list.get(i));
				break;
			}

		}
	}


	public boolean isVideoTimeStampPresent() {
		// TODO Auto-generated method stub
		boolean videoTimeStamp = waitForElementPresence(vidTimeStamp);
		return videoTimeStamp ;
	}


	public void clickExoOverlay() throws Exception {
		click(exo_overlay);
		click(pause);

	}


	public boolean compareVideoTimeStamps() throws InterruptedException, ParseException {
		// TODO Auto-generated method stub
		String videoTimeStamp1= getText(vidTimeStamp);
		videoTimeStamp1=videoTimeStamp1.split(" ")[3];

		System.out.println(videoTimeStamp1);
		String videoTimeStamp2= getText(vidTimeStamp);
		videoTimeStamp2=videoTimeStamp2.split(" ")[3];
		return compareDateTime(videoTimeStamp1,videoTimeStamp2);

	}


	public boolean sentChatVerifcation() throws Exception {
		if(is_empty(chatId)==true)
		{
			String temp = captureScreen(androidDriver);
			test.log(Status.FAIL, "details", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
			test.log(Status.FAIL, "No Chat Displayed");
			System.out.println("No Chat Displayed");
			return false;
		}
		else
		{
			int chatscount = elemetns_Size(chatId);
			System.out.println("After list size");
			String lastChat=getText(chatId.get(chatscount-1));
			String penultimateChat = getText(chatId.get(chatscount-2));
			if(lastChat.equalsIgnoreCase(penultimateChat) && lastChat.equalsIgnoreCase(penultimateChat))
			return true;
		}
		return false;
	}




}
