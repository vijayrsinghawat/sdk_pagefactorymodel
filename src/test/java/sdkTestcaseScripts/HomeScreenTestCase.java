package sdkTestcaseScripts;

import java.io.IOException;

import org.testng.annotations.Test;

import sdk.FunctionLibrary;
import sdkTestappModuleElements.HomeScreenElements;


public class HomeScreenTestCase extends FunctionLibrary{

	HomeScreenElements home;

	@Test(priority=1)
	public void isDrawerLayoutDisplayed() throws InterruptedException, IOException {

		System.out.println("DrawerLayout Script Running");
		try
		{
			test = extent.createTest("TestCase: isDrawerLayoutDisplayed");
		}
		catch(Exception e)
		{
			test.fail("Exception at test: "+e);
		}
		home=new HomeScreenElements(androidDriver);
		home.alertOk();
		home.clickOnDrawerLayout();
		if(home.isEditTextboxDisplayed()==true)
		{
			System.out.println("DrawerLayout Displayed successfully");
			boolean flag = true;
			FunctionLibrary.verifyText(flag,true,"DrawerLayout Displayed Successfully.", test);

		}
		else
		{
			System.out.println("DrawerLayout not Displayed");
			boolean flag = false;
			FunctionLibrary.verifyText(flag,true,"DrawerLayout Not loaded Successfully.", test);

		}

		//LOGGER.info("Waiting 10 seconds then turning...");
	}

	@Test(priority=2)
	public void isOverLayLayoutDisplayed() throws Exception {


		home=new HomeScreenElements(androidDriver);
		FunctionLibrary.appRelaunch();
		System.out.println("OverlayLayout Script Running");
		try
		{
			test = extent.createTest("TestCase: isOverLayLayoutDisplayed");
		}
		catch(Exception e)
		{
			test.fail("Exception at test: "+e);
		}
		home.clickOverLayLayout();
		if(home.isEditTextboxDisplayed()==true)
		{
			System.out.println("OverlayLayout Displayed successfully");
			boolean flag = true;
			FunctionLibrary.verifyText(flag,true,"OverlayLayout Displayed Successfully!", test);

		}
		else
		{
			System.out.println("OverlayLayout not Displayed");
			boolean flag = false;
			FunctionLibrary.verifyText(flag,true,"OverlayLayout Not loaded Successfully.", test);

		}


		//LOGGER.info("Waiting 10 seconds then turning...");
	}


	@Test(priority=3)
	public void isEventsDisplayed() throws Exception {

		home=new HomeScreenElements(androidDriver);
		FunctionLibrary.appRelaunch();
		Thread.sleep(3000);
		System.out.println("Events_list Script Running");
		try
		{
			test = extent.createTest("TestCase: isEventsDisplayed");
		}
		catch(Exception e)
		{
			test.fail("Exception at test: "+e);
		}
		home.clickOnEvents();

		if(home.isChannelListEmpty()==false)
		{
			System.out.println("Evetns list Displayed successfully");
			boolean flag = true;
			FunctionLibrary.verifyText(flag,true,"Evetns list Displayed Successfully.", test);
            
		}
		else
		{
			System.out.println("Evetns list not Displayed");
			boolean flag = false;
			FunctionLibrary.verifyText(flag,true,"Evetns list Not loaded Successfully.", test);
		}

		//LOGGER.info("Waiting 10 seconds then turning...");
	}

	@Test(priority=4)
	public void isWidgetOnlyTabDisplayed() throws Exception {
		home=new HomeScreenElements(androidDriver);
		FunctionLibrary.appRelaunch();
		try
		{
			test = extent.createTest("TestCase: isWidgetOnlyTabDisplayed");
		}
		catch(Exception e)
		{
			test.fail("Exception at test: "+e);
		}
		System.out.println("WidgenOnly Script Running");
		home.clickOnWidgetsOnly();

		if(home.isWidgetOnlyListEmpty()==false)
		{
			boolean flag = true;
			FunctionLibrary.verifyText(flag,true,"WidgetOnly-Tab Successfully.", test);
			System.out.println("Widgets list Displayed successfully");
		}
		else
		{
			boolean flag = false;
			FunctionLibrary.verifyText(flag,true,"WidgetOnly-Tab Not loaded Successfully.", test);
			System.out.println("Widgets list not Displayed");
		}

		//LOGGER.info("Waiting 10 seconds then turning...");
	}

	@Test(priority=5)
	public void isChatOnlyTabDisplayed() throws Exception {
		home=new HomeScreenElements(androidDriver);
		FunctionLibrary.appRelaunch();
		try
		{
			test = extent.createTest("TestCase: isChatOnlyTabDisplayed");
		}
		catch(Exception e)
		{
			test.fail("Exception at test: "+e);
		}
		System.out.println("ChatOnly Script Running");
		home.clickOnChatOnly();

		if(home.isEditTextboxDisplayed()==true)
		{
			boolean flag = true;
			FunctionLibrary.verifyText(flag,true,"ChatOnly-Tab Successfully.", test);
			System.out.println("ChatOnly Screen Displayed successfully");
		}
		else {
			boolean flag = false;
			FunctionLibrary.verifyText(flag,true,"ChatOnly-Tab Not Loaded Successfully.", test);
			System.out.println("ChatOnly Screen does not Displayed ");
		}

		//LOGGER.info("Waiting 10 seconds then turning...");
	}



}
