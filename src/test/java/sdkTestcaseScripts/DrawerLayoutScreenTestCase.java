package sdkTestcaseScripts;

import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;

import dataProvider.ConfigFileReader;
import sdk.FunctionLibrary;
import sdkTestappModuleElements.AllLayoutScreenElements;

public class DrawerLayoutScreenTestCase extends FunctionLibrary{
	AllLayoutScreenElements drawerLayout ;

	/*@Test(priority=1)
	public void selectChannel() throws Exception {
		//screenshot("turningTest1");
		System.out.println("DrawerLayout: SelectChannels Script Running");
		try
		{
			test = extent.createTest("TestCase: SelectChannels");
		}
		catch(Exception e)
		{
			test.fail("Exception at test: "+e);
		}
		drawerLayout=new AllLayoutScreenElements(androidDriver);
		drawerLayout.clickOnDrawerLayout();
		drawerLayout.click_selectChannelButton();


		if(drawerLayout.isChannelListEmpty()==false)
		{
			System.out.println("EventsList Displayed successfully");
			boolean flag = true;
			FunctionLibrary.verifyText(flag,true,"Events list is present in drawerlayout", test);
			drawerLayout.clickOnSyncChannel();
			drawerLayout.click_selectChannelButton();
			drawerLayout.clickOnNonSyncChannel();
			drawerLayout.click_selectChannelButton();
			drawerLayout.clickOnSyncChannel();
			test.log(Status.PASS,"<font size=3 color=\"#3ADF00\">" + "Channels Changed Successfully"+ "</font>");
			Thread.sleep(3000);
			String temp = captureScreen(androidDriver);
			test.log(Status.PASS, "details", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());

		}
		else
		{
			System.out.println("EventsList not Displayed");
			boolean flag = false;
			FunctionLibrary.verifyText(flag,true,"Events list is not present in drawerlayout", test);

		}
	}


	@Test(priority=2)
	public void isVideoPlaying() throws Exception {
		//screenshot("turningTest1");
		FunctionLibrary.appRelaunch();
		System.out.println("DrawerLayout: isVideoPlaying Script Running");
		try
		{
			test = extent.createTest("TestCase: isPlayPauseButtonWorking");
		}
		catch(Exception e)
		{
			test.fail("Exception at test: "+e);
		}
		drawerLayout=new AllLayoutScreenElements(androidDriver);
		drawerLayout.clickOnDrawerLayout();
		drawerLayout.click_selectChannelButton();
		drawerLayout.clickOnSyncChannel();
		Thread.sleep(3000);
		String temp = captureScreen(androidDriver);
		test.log(Status.PASS, "details", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
		if(drawerLayout.compareVideoTimeStamps())
		{
			boolean flag = true;
			FunctionLibrary.verifyText(flag,true,"Video is playing", test);
			System.out.println("Pass");
		}
		else
		{
			boolean flag = false;
			FunctionLibrary.verifyText(flag,true,"Video is not playing", test);
			System.out.println("Fail");
		}
		//if(drawerLayout.isVideoTimeStampPresent())
		//{
		//System.out.println("Inside Videostream");
		//drawerLayout.clickExoPauseButton();
		//Thread.sleep(5000);
		//}
        }

	@Test(priority=3)
	public void isChatSent() throws Exception {
		//screenshot("turningTest1");
		FunctionLibrary.appRelaunch();
		System.out.println("DrawerLayout: isChatSent Script Running");
		try
		{
			test = extent.createTest("TestCase: isChatDisplayed on the screen");
		}
		catch(Exception e)
		{
			test.fail("Exception at test: "+e);
		}
		drawerLayout=new AllLayoutScreenElements(androidDriver);
		drawerLayout.clickOnDrawerLayout();
		drawerLayout.click_selectChannelButton();
		drawerLayout.clickOnSyncChannel();
		Thread.sleep(3000);
		String temp = captureScreen(androidDriver);
		test.log(Status.PASS, "details", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
		FunctionLibrary.sendInputOnChatScreen(con.device_Udid(),con.getText());

		if(drawerLayout.sentChatVerifcation())
		{
			boolean flag = true;
			FunctionLibrary.verifyText(flag,true,"Chat Sent Successfully", test);
		}
		else
		{
			boolean flag = false;
			FunctionLibrary.verifyText(flag,true,"Chat Not Sent Successfully", test);
		}

	}*/


}
