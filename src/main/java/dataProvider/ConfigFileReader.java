package dataProvider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {

	private Properties properties;
	private final String propertyFilePath= "src/main/resources/config.properties";


	public ConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Config.properties not found at " + propertyFilePath);
		} 
	}


	public long getImplicitlyWait() { 
		String implicitlyWait = properties.getProperty("implicitlyWait");
		if(implicitlyWait != null) return Long.parseLong(implicitlyWait);
		else throw new RuntimeException("ImplicitlyWait not specified in the config.properties file."); 
	}

	public String getApplicationUrl() {
		String url = properties.getProperty("url");
		if(url != null) return url;
		else throw new RuntimeException("Url not specified in the config.properties file.");
	}

	public String getApplcationName() {
		String app_name = properties.getProperty("APP_NAME");
		if(app_name != null) return app_name;
		else throw new RuntimeException("APP_NAME not specified in the config.properties file.");
	}

	public boolean getNoResetValue() {
		boolean noreset = properties.getProperty("NoReset") != null;
		return noreset;

	}

	public String getDeviceName() {
		String devicename = properties.getProperty("deviceName");
		System.out.println(devicename);
		if(devicename != null) return devicename;
		else throw new RuntimeException("DeviceName not specified in the config.properties file.");
	}

	public String device_Udid() {
		String d_udid = properties.getProperty("udid");
		
		if(d_udid != null) return d_udid;
		else throw new RuntimeException("Udid not specified in the config.properties file.");
	}

	public String platform_Version(){
		String d_platform_version = properties.getProperty("platformVersion");
		if(d_platform_version!= null) return d_platform_version;
		else throw new RuntimeException("Platform_version not specified in the config.properties file."); 
	}

	public String platformName(){
		String platformName = properties.getProperty("platformName");
		if(platformName!= null) return platformName;
		else throw new RuntimeException("PlatformName not specified in the config.properties file."); 
	}

	public String app_Package(){
		String application_Package = properties.getProperty("appPackage");
		if(application_Package!= null) return application_Package;
		else throw new RuntimeException("App_Package not specified in the config.properties file."); 
	}

	public String app_Activity(){
		String application_Activity = properties.getProperty("appPackage");
		if(application_Activity!= null) return application_Activity;
		else throw new RuntimeException("App_activity not specified in the config.properties file."); 
	}

	public boolean get_autoGrantPermissions() {
		boolean autoGrantPermissions = properties.getProperty("autoGrantPermissions")!= null;
		return autoGrantPermissions;
	}

	public boolean get_autoAcceptAlerts() {
		boolean autoAcceptAlerts = properties.getProperty("autoAcceptAlerts")!= null;
		return autoAcceptAlerts;
	}

	public boolean get_useNewWDA() {
		boolean useNewWDA = properties.getProperty("useNewWDA")!= null;
		return useNewWDA;
	}

	public boolean get_clearSystemFiles() {
		boolean clearSystemFiles = properties.getProperty("clearSystemFiles")!= null;
		return clearSystemFiles;
	}

	public int get_newCommandTimeout() {
		String newCommandTimeout_result = properties.getProperty("newCommandTimeout");
		int newCommandTimeout = Integer.parseInt(newCommandTimeout_result);
		return newCommandTimeout;
	}


	public String get_IPAddress(){
		String IPAddress = properties.getProperty("IPAddress");
		if(IPAddress!= null) return IPAddress;
		else throw new RuntimeException("IPAddress not specified in the config.properties file."); 
	}

	public int get_port(){
		String result = properties.getProperty("port").trim();
		
		int port = Integer.parseInt(result);
		System.out.println("result:::"+port);
		if(port!=0)return port;
		else throw new RuntimeException("Port not specified in the config.properties file."); 
	}
	
	public String getUdid() {
		String udid = properties.getProperty("udid");
		if(udid != null) return udid;
		else throw new RuntimeException("Udid not specified in the config.properties file.");
	}
	
	public String getText() {
		String text= properties.getProperty("text");
		if(text != null) return text;
		else throw new RuntimeException("Udid not specified in the config.properties file.");
	}


}
