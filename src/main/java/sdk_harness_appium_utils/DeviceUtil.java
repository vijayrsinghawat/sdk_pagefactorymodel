package sdk_harness_appium_utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Device setup utilities
 */
public class DeviceUtil {

	private static Logger LOGGER = LoggerFactory.getLogger(DeviceUtil.class);

	/**
	 * For debugging purposes dump connected device information using adb system command.
	 */
	public static void adbDeviceInfo() {
		LOGGER.info("Running adbDeviceInfo...");

		try {
			Process iostat = new ProcessBuilder()
					.command("adb", "devices")
					.inheritIO().start();
			LOGGER.info("adb devices exited: " + iostat.waitFor());

			iostat = new ProcessBuilder()
					.command("adb", "shell",  "ifconfig", "wlan0")
					.inheritIO().start();
			LOGGER.info("adb shell exited: " + iostat.waitFor());
		} catch (IOException | InterruptedException e) {
			LOGGER.error(e.getMessage());
		}
	}

}
