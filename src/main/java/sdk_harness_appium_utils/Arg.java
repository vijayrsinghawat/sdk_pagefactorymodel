package sdk_harness_appium_utils;

import io.appium.java_client.service.local.flags.ServerArgument;

public enum Arg implements ServerArgument {

	Relaxed_Security("--relaxed-security");
	private final String arg;
	Arg(String arg)
	{
		this.arg=arg;
	}

	@Override
	public String getArgument() {
		// TODO Auto-generated method stub
		return arg;
	}



}
